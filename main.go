package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
)

func main() {
	http.HandleFunc("/", routeIndexGet)
	http.HandleFunc("/upload", routeSubmitPost)

	fmt.Println("server started at localhost:9000")
	http.ListenAndServe(":9000", nil)
}

func routeIndexGet(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		var tmpl = template.Must(template.New("form").ParseFiles("index.html"))
		var err = tmpl.Execute(w, nil)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	http.Error(w, "", http.StatusBadRequest)
}
func routeSubmitPost(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		token := r.FormValue("auth")
		if token != "token12345" {
			w.WriteHeader(http.StatusForbidden)
			w.Write([]byte("403 HTTP status code returned!"))
			return
		}
		// Parse our multipart form, 8 << 20 specifies a maximum
		// upload of 8 MB files.
		r.ParseMultipartForm(8 << 20)
		//get name "file"
		file, handler, err := r.FormFile("file")
		if err != nil {
			fmt.Println("Error Retrieving the File")
			fmt.Println(err)
			return
		}
		defer file.Close()
		// detect file is not images
		buff := make([]byte, 512)
		_, err = file.Read(buff)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		filetype := http.DetectContentType(buff)
		if filetype != "image/jpeg" && filetype != "image/png" {
			http.Error(w, "The provided file format is not allowed. Please upload a JPEG or PNG image", http.StatusBadRequest)
			return
		}

		//temp folder
		tempFile, err := ioutil.TempFile("temp-images", "images-*.png")
		if err != nil {
			fmt.Println(err)
		}
		defer tempFile.Close()

		// read all of the contents of our uploaded file into a
		// byte array
		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			fmt.Println(err)
		}
		// write this byte array to temporary file
		tempFile.Write(fileBytes)
		// return that we have successfully uploaded our file!
		fmt.Fprintf(w, "Successfully Uploaded File\n")
		fmt.Println("Uploaded File: ", handler.Filename)
		fmt.Println("File Size: ", handler.Size)
		fmt.Println("MIME Header: ", handler.Header)
		return
	}

	http.Error(w, "", http.StatusBadRequest)
}
